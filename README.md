## NextCloud

Ce dossier contient les ressources nécessaires pour lancer une ou plusieurs instances NextCloud.

### Configuration

Quasiment aucune configuration n'est effectuée via les fichiers de ce dépôt, et on préfère l'interface web.
Le défaut est qu'il n'est pas possible de lancer des instances NextCloud **vraiment** personnalisées depuis ce dépôt, mais c'est parce que le format des fichiers de configuration est amené à évoluer et que NextCloud effectue des migrations automatiques lors des mises à jour.

Versionner les fichiers de configuration serait donc en conflit avec les modifications automatiques effectuées par NextCloud lors des mises à jour et des changements dans l'interface.

Les fichiers `nginx.conf` sont repris de [cet exemple](https://github.com/nextcloud/docker/blob/master/.examples/docker-compose/with-nginx-proxy/postgres/fpm/web/nginx.conf).

### Lancement

Copier les fichiers `.secrets.example` en `.secrets` et remplacer les valeurs.
Lancer `docker-compose up -d`.

### Personnalisation

Le thème se configure directement via les paramètres de l'application, en tant qu'administrateur.
Pour ce faire, se rendre dans Paramètres → Personnaliser l'apparence.

Le fichier [logo.png](./logo.png) est utilisé pour le Logo et le Logo d'en-tête.
Le fichier  [favicon.png](./favicon.png) est utilisé pour le Favicon.

Les CGU sont celles de Picasoft : https://picasoft.net/co/cgu.html

La couleur de fond est `#8CBD8C`.

### Mise à jour de Nextcloud

Pour mettre à jour l'instance de Picasoft, il suffit de mettre à jour le tag de l'image officielle de NextCloud.

Attention : **toutes les mises à jour de version majeure doivent se faire une par une**. Les logs applicatifs détaillent la mise à jour.
Exemple :
* 15 -> 16, puis
* 16 -> 17, puis
* 17 -> 18.

Sinon, il y a risque de casse.

### Mise à jour de PostgreSQL

Il peut arriver que la version de PostgreSQL ne soit plus supportée par NextCloud.
Sans en arriver là, il est bon de régulièrement mettre à jour PostgreSQL :
> While upgrading will always contain some level of risk, PostgreSQL minor releases fix only frequently-encountered bugs, security issues, and data corruption problems to reduce the risk associated with upgrading. For minor releases, the community considers not upgrading to be riskier than upgrading. https://www.postgresql.org/support/versioning/

Les mise à jours mineures (changement du Y de la version X.Y) peuvent se faire sans intervention humaine. On veillera à bien regarder les logs.

En revanche, le passage d'une version majeure à une autre nécessitera une intervention manuelle.

La documentation complète est ici : https://www.postgresql.org/docs/current/upgrading.html

De manière générale, la façon la plus simple est de se rendre dans l'ancien conteneur, de réaliser un `pg_dumpall` et de le copier en lieu sûr (`docker cp`).
Ensuite, on supprime l'ancien volume de base de données, on relance le nouveau conteneur de base de données (qui sera sans donnée), on monte le fichier de dump, et on lance un `psql -U <user> -d <db> -f <dump_file>` (valeurs de `user` et `db` à matcher avec le fichiers de secrets).

On attend, et **si tout s'est bien passé**, on peut lancer le conteneur applicatif (NextCloud).
